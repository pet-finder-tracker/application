import axios from "axios";

export const request = axios.create({
  // baseURL: 'http://192.168.43.106:8000/api/v1/',
  baseURL: 'https://pets-finder-server.herokuapp.com/api/v1/',
});

request.interceptors.response.use(response => response.data);