import { createStackNavigator } from 'react-navigation';

import { theme } from "../theme";
import { MyPetsScreen } from "../screens";

const MyPetsStack = createStackNavigator({
  Search: MyPetsScreen,
}, {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: theme.colors.primary,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  });

export default MyPetsStack;