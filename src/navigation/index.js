import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';

import TabNavigator from "./Tabs";
import { LoginScreen } from "../screens";
// Implementation of HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen
// goes here.

const AuthStack = createStackNavigator({
  SignIn: LoginScreen,
}, {
    headerMode: 'none',
  });

export default createAppContainer(createSwitchNavigator(
  {
    // AuthLoading: AuthLoadingScreen,
    App: TabNavigator,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'Auth',
    headerMode: 'none',
  }
));