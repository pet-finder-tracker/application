import React from 'react';
import { createBottomTabNavigator, createAppContainer } from "react-navigation";
import Ionicons from 'react-native-vector-icons/Ionicons';

import { EngageScreen, SettingsScreen } from "../screens";
import SearchStack from "./Search";
import MyPetsStack from "./MyPets"
import { theme } from '../theme';

const MainNavigator = createBottomTabNavigator({
  Search: SearchStack,
  Engage: EngageScreen,
  MyPets: MyPetsStack,
  Settings: SettingsScreen,
}, {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Search') {
          iconName = `md-search`;
        } else if (routeName === 'Engage') {
          iconName = `md-heart`;
        } else if (routeName === 'MyPets') {
          iconName = `md-paw`;
        } else if (routeName === 'Settings') {
          iconName = `md-settings`;
        }
        return <IconComponent name={iconName} size={focused ? 25 : 20} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      showLabel: false,
      activeTintColor: theme.colors.secondary,
      inactiveTintColor: theme.colors.secondaryInactive,
      activeBackgroundColor: theme.colors.primary,
      inactiveBackgroundColor: theme.colors.primary,
    },
  });

export default createAppContainer(MainNavigator);