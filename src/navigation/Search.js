import { createStackNavigator } from 'react-navigation';

import { theme } from "../theme";
import { SearchScreen, PostScreen } from "../screens";

const SearchStack = createStackNavigator({
  Search: SearchScreen,
  Post: PostScreen,
}, {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: theme.colors.primary,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  });

export default SearchStack;