import { StyleSheet } from 'react-native'
import { theme } from "../../theme";

export default StyleSheet.create({
  container: {
    marginTop: 5,
    marginBottom: 5,
    marginRight: 3,
    height: "50%",
    backgroundColor: theme.colors.background,
    shadowColor: "#FF0000",
    elevation: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
  }
});
