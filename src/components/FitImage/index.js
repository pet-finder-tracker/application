import React from 'react';

import { withTheme } from "../../theme";
import { View, Image } from "../";
import styles from "./styles";

type Props = {};
class FitImageComponent extends React.Component<Props> {

  render() {
    const { image } = this.props;
    return (
      <View style={styles.container}>
        <Image style={styles.image} width={"100%"} height={"100%"} source={{ uri: image }}></Image>
      </View>
    )
  }
}

export default withTheme(FitImageComponent);