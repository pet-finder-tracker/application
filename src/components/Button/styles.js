import { StyleSheet } from 'react-native'
import { theme } from "../../theme";

export default StyleSheet.create({
  button: {
    alignItems: 'center',
    padding: 15,
    width: '100%',
    marginTop: '5%',
    marginBottom: '3%',
    borderRadius: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 5,
    elevation: 8,
  },
  text: {
    fontSize: 17,
  }
});
