import React from 'react';
import { TouchableOpacity } from "react-native";

import { withTheme } from "../../theme";
import { Text } from "../";
import styles from "./styles";

type Props = {};
class Button extends React.Component<Props> {
  constructor(props) {
    super(props);

    this.setColor();
  }

  setColor() {
    if (this.props.secondary) {
      this.color = this.props.theme.colors.secondary;
      this.text = this.props.theme.colors.secondaryText;
    } else {
      this.color = this.props.theme.colors.primary;
      this.text = this.props.theme.colors.primaryText;
    }
  }

  render() {
    return (
      <TouchableOpacity
        style={{ backgroundColor: this.color, ...styles.button, ...this.props.style }}
        onPress={this.props.onPress}
      >
        <Text
          style={{ color: this.text, ...styles.text, ...this.props.textStyle }}
        >
          {this.props.text}
        </Text>
      </TouchableOpacity>
    )
  }
}

export default withTheme(Button);