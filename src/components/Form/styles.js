import { StyleSheet } from 'react-native'

import { theme } from "../../theme";

export default StyleSheet.create({
  container: {
    padding: '5%',
    height: '100%',
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    // justifyContent: 'space-around',
    // alignItems: 'center',
  },
});
