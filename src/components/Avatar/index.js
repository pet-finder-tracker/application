import React from "react";
import Icon from "react-native-vector-icons/MaterialIcons";

import { View, Image } from "../";
import styles from './styles';
import { theme } from "../../theme";

export default class AvatarComponent extends React.Component {

  render() {
    return (
      <View style={{ ...styles.avatar, ...this.props.style }}>
        {this.props.image ?
          <Image style={styles.image} source={{ uri: this.props.image }}></Image> :
          <Icon size={this.props.iconSize} color={theme.colors.primary} name={this.props.name}></Icon>
        }
      </View>
    )
  }
}