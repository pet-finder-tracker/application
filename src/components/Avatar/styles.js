import { StyleSheet } from 'react-native'

import { theme } from "../../theme";

export default StyleSheet.create({
  avatar: {
    borderRadius: 200,
    height: 250,
    width: 250,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.secondary,
    borderWidth: 0,
  },
  image: {
    borderRadius: 200,
    borderWidth: 0,
    height: '100%',
    width: '100%'
  }
});
