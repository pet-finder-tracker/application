import { StyleSheet } from 'react-native'

import { theme } from "../../theme";

export default StyleSheet.create({
  container: {
    margin: 5,
    padding: 2,
    width: "100%",
    alignItems: 'center'
  },
  textInput: {
    width: '80%',
    color: theme.colors.primary,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.primary,
  }
});
