import React, { Component } from 'react';
import { View, Animated, TextInput } from 'react-native';
import { string, func } from 'prop-types';

import styles from "./styles";
import { withTheme } from '../../theme';

class Input extends Component {
  // static propTypes = {
  //   placeholder: string.isRequired,
  //   value: string.isRequired,
  //   onChange: func.isRequired,
  // }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          {...this.props}
          ref={input => this.props.inputRef && this.props.inputRef(input)}
          style={styles.textInput}
        />
      </View>
    )
  }
}

export default withTheme(Input);
