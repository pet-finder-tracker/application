import { StyleSheet } from 'react-native'

import { theme } from "../../theme";

export default StyleSheet.create({
  container: {
    padding: '5%',
    paddingBottom: 0,
    height: '100%',
    backgroundColor: theme.colors.background,
  },
});
