import React from 'react';

import { withTheme } from "../../theme";
import { View, Text, Avatar } from "../";
import styles from "./styles";

const LateralMark = (_color) => {
  let color = 'rgba(225, 225, 0, 0.5)';
  switch (_color.color) {
    case 1: // Lost
      color = 'rgba(225, 0, 0, 0.5)';
      break;
    case 2: // Found
      color = '#rgba(0, 225, 0, 0.5)';
      break;
    case 3: // Ad
      color = '#rgba(150, 150, 150, 0.5)'
      break;
  }
  return (
    <View
      style={{ ...styles.lateralMark, backgroundColor: color }}
    ></View>
  )
};

type Props = {};
class CardComponent extends React.Component<Props> {

  render() {
    const { data } = this.props;
    console.log(data)
    return (
      <View style={styles.container}>
        <LateralMark color={data.type}></LateralMark>
        <View style={styles.body}>
          <Avatar style={styles.avatar} image={data.image}></Avatar>
          <View style={styles.text}>
            <Text style={styles.title}>{data.title}</Text>
            <Text style={styles.description}>{data.description}</Text>
          </View>
        </View>
      </View>
    )
  }
}

export default withTheme(CardComponent);