import { StyleSheet } from 'react-native'
import { theme } from "../../theme";

export default StyleSheet.create({
  container: {
    marginTop: 5,
    marginBottom: 5,
    marginRight: 3,
    height: 160,
    backgroundColor: theme.colors.background,
    shadowColor: "#FF0000",
    elevation: 4,
    flexDirection: 'row', 
  },
  body: {
    padding: 2,
    width: '99%',
    flexDirection: 'row', 
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  lateralMark: {
    width: 5,
    height: '100%',
  },
  avatar: {
    backgroundColor: theme.colors.background,
    height: 110,
    width: 110,
    marginLeft: 2.5,
    shadowColor: "#FF0000",
    elevation: 8,
  },
  text: {
    width: '65%',
    height: '100%',
    position: 'relative',
    padding: 1,
    paddingLeft: 15,
    borderLeftWidth: 1,
    borderColor: '#CCCCCC',
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginTop: 3,
    marginBottom: 3,
  },
  description: {
    textAlign: 'justify',
  },
});
