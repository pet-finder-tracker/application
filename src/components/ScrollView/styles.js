import { StyleSheet } from 'react-native'

import { theme } from "../../theme";

export default StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: theme.colors.background,
  },
});
