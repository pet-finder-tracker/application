import React from "react";
import { ScrollView } from "react-native";

import styles from './styles';

export default class ScrollViewComponent extends React.Component {
  render() {
    return (
      <ScrollView style={{ ...styles.container, ...this.props.style }}>
        {this.props.children}
      </ScrollView>
    )
  }
}