import React, { Component } from 'react';

import styles from './styles'
import { ScreenView, Text, Button } from '../../components'

type Props = {};
export default class SettingsScreen extends Component<Props> {
  constructor(props) {
    super(props);

    this.logout = this.logout.bind(this);
  }

  logout() {
    this.props.navigation.navigate('Auth');
  }
  render() {
    return (
      <ScreenView>
        <Text>Settings</Text>
        <Button text="Cerrar sesión" onPress={this.logout} />
      </ScreenView>
    );
  }
}