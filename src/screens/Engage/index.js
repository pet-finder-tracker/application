import React, { Component } from 'react';

import wipEngage from '../../assets/wip_engage.png'
import styles from './styles'
import { ScreenView, Text, ImageBackground } from '../../components'

type Props = {};
export default class EngageScreen extends Component<Props> {
  render() {
    return (
      <ScreenView style={styles.container}>
        <ImageBackground source={wipEngage} style={{ height: '60%' }}></ImageBackground>
        <Text style={styles.instructions}>Pronto podrás buscar pareja para tu mascota!</Text>
      </ScreenView>
    );
  }
}