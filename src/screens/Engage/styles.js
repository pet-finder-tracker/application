import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    height: '100%',
  },
  instructions: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 25
  }
});
