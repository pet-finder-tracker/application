import React, { Component } from 'react';

import styles from './styles'
import { ScreenView, View, Text, Avatar } from '../../components'

type Props = {};
export default class MyPetsScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Mis Mascotas',
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      data: {
        name: 'Tuti',
        sex: 'm',
        breed: 'Kelpie',
        age: 4,
        image: 'https://images.dog.ceo/breeds/kelpie/n02105412_7256.jpg',
      }
    }
  }

  render() {
    let { data } = this.state;
    return (
      <ScreenView styles={styles.container}>
        <View style={styles.card}>
          <View style={styles.body}>
            <Avatar style={styles.avatar} image={data.image}></Avatar>
            <View style={styles.text}>
              <Text style={styles.title}>{data.name}</Text>
              <Text style={styles.description}>Raza: {data.breed}</Text>
              <Text style={styles.description}>Edad: {data.age}</Text>
              <Text style={styles.description}>Sexo: {data.breed == 'm' ? 'Macho' : 'Hembra'}</Text>
            </View>
          </View>
        </View>
      </ScreenView>
    );
  }
}