import { StyleSheet } from 'react-native'
import { theme } from '../../theme';

export default StyleSheet.create({
  image: {
    height: 30,
    width: 100,
    borderWidth: 1,
    borderColor: 'red',
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    paddingTop: 15,
    paddingBottom: 15,
    color: theme.colors.secondaryText,
  },
  description: {
    fontSize: 20,
    color: theme.colors.secondaryText,
  },
  button: {
    bottom: 0,
  },
  container: {
    height: "37%",
  }
});
