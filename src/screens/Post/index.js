import React, { Component } from 'react';

import styles from './styles'
import { ScreenView, View, Text, Button, FitImage } from '../../components'

type Props = {};
export default class PostScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Post',
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.getParam('data'),
    };

    this._renderButton = this._renderButton.bind(this)

  }
  _renderButton() {
    switch (this.state.data.type) {
      case 1:
        return (
          <Button style={styles.button} text={'Lo Encontré!'}></Button>
        )
      case 2:
        return (
          <Button style={styles.button} text={'Es Mío!'}></Button>
        )
      case 3:
        return (
          <Button secondary style={styles.button} text={'Lo Quiero!'}></Button>
        )
    }

  }


  render() {
    return (
      <ScreenView>
        <FitImage style={styles.image} image={this.state.data.image}></FitImage>
        <View style={styles.container}>
          <Text style={styles.title}>{this.state.data.title}</Text>
          <Text style={styles.description}>{this.state.data.description}</Text>
        </View>
        {this._renderButton()}
      </ScreenView>
    );
  }
}