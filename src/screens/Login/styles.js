import { StyleSheet } from 'react-native'

import { theme } from "../../theme";

export default StyleSheet.create({
  container: {
    backgroundColor: theme.colors.background,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingTop: 50,
  },
  buttonContainer: {
    width: "100%",
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});
