import React, { Component } from 'react';

import styles from './styles'
import { View, Input, Button, Form, Avatar, KeyboardAvoidingView } from '../../components'
import { request } from "../../api";

type Props = {};
class LoginScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        email: 'fballoncuadros@gmail.com',
        password: 'Test12345',
      },
    };

    this.login = this.login.bind(this);
  }

  login() {
    request.post('user/login/', this.state.form).then(({ authToken }) => {
      request.interceptors.request.use(config => {
        Object.keys(config.headers).map(k => config.headers[k]['Authorization'] = `${authToken}`);
        return config;
      });
      this.props.navigation.navigate('App');
    }).catch(err => {
      console.error(err);
      alert("Please verify your credentials");
    })
  }

  render() {
    return (
      <KeyboardAvoidingView behavior='height' enabled style={styles.container}>
        <Avatar iconSize={175} name='pets'></Avatar>
        <Form>
          <View style={{ width: '100%' }}>
            <Input
              keyboardType="email-address"
              textContentType="emailAddress"
              placeholder="Email"
              blurOnSubmit={false}
              onSubmitEditing={() => this.passwordRef.focus()}
              returnKeyType="next"
              value={this.state.form.email}
              onChangeText={v => this.setState({ form: { ...this.state.form, email: v } })}
            ></Input>
            <Input
              secureTextEntry
              placeholder="Password"
              inputRef={(ref) => { this.passwordRef = ref; }}
              returnKeyType="go"
              value={this.state.form.password}
              onChangeText={v => this.setState({ form: { ...this.state.form, password: v } })}
            ></Input>
          </View>
          <View style={styles.buttonContainer}>
            <Button text="Iniciar sesión" onPress={this.login} />
            <Button secondary text="Registrar" onPress={this.login} />
          </View>
        </Form>
      </KeyboardAvoidingView>
    );
  }
}

export default LoginScreen;