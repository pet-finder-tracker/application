import React, { Component } from 'react';
import { Dimensions } from "react-native";

import styles from './styles'
import { FlatList, ScreenView, Card, TouchableOpacity } from '../../components';
import { request } from '../../api';

type Props = {};
export default class SearchScreen extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Search',
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 0,
    };

    this.renderCard = this.renderCard.bind(this);
    this._openCard = this._openCard.bind(this);
    this._handleLoadMore = this._handleLoadMore.bind(this);
  }

  componentDidMount() {
    this._handleLoadMore();
  }

  renderCard(item) {
    console.log(item)
    return (
      <TouchableOpacity onPress={() => this._openCard(item)}>
        <Card key={item._id} data={item}></Card>
      </TouchableOpacity>
    )
  }

  _openCard(item) {
    this.props.navigation.navigate('Post', { data: item });
  }

  _handleLoadMore() {
    request.get(`posts/?page=${this.state.page}`).then(posts => {
      console.log(posts);
      this.state.data.push(...posts);
      this.setState({ ...this.state, page: this.state.page + 1 });
    }).catch(err => {
      console.log(err);
    })
  }

  render() {
    const dimensions = Dimensions.get('window');
    const screenWidth = dimensions.width - 35;
    return (
      <ScreenView styles={styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          style={{
            flex: 1,
            width: screenWidth,
          }}
          data={this.state.data}
          renderItem={({ item }) => this.renderCard(item)}
          keyExtractor={(item, index) => index}
          onEndReached={this._handleLoadMore}
          onEndReachedThreshold={0.5}
          initialNumToRender={10}
        />
      </ScreenView>
    );
  }
}