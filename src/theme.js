import { createTheming } from '@callstack/react-theme-provider';

export let theme = {
  colors: {
    primary: "#574339",
    secondary: "#F1C40F",
    secondaryInactive: '#C09C0C',
    error: "#F73E22",
    background: "#F4ECDE",
    action: "#0285CC",
    secondaryText: '#111111',
    primaryText: '#EEEEEE',
  }
};

const { ThemeProvider, withTheme } = createTheming(theme);

export { ThemeProvider, withTheme };