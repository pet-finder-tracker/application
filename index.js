import React, { Component } from "react";
import { AppRegistry } from 'react-native';

import MainNavigator from "./src/navigation";
import { name as appName } from './app.json';
import { ThemeProvider, theme } from './src/theme'

class MainView extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <MainNavigator />
      </ThemeProvider>
    )
  }
}

AppRegistry.registerComponent(appName, () => MainView);
